#!/bin/bash

source /i9mlib-6.0.sh

: ${ACME_EMAIL="email@example.com"}
: ${LB_POLICY="round_robin"}
: ${HEALTH_CHECK="/"}

if [[ "${ENABLE_HTTPS}" == "true" ]]; then
  echo "
{
  email ${ACME_EMAIL}

  storage redis {
    address       \"redis-${NAMESPACE}:6379\"
    db            0
    tls_enabled   \"false\"
    tls_insecure  \"true\"
  }
}
" > /etc/caddy/Caddyfile
else
  echo "" > /etc/caddy/Caddyfile
  DOMAIN_NAME="http://"
fi

echo "
${DOMAIN_NAME} {
  reverse_proxy {
" >> /etc/caddy/Caddyfile

if [[ "${UPSTREAMS_TYPE}" == "DYNAMIC" ]]; then
  first_server=$(echo $SERVERS | awk '{print $1}')

  if [[ $first_server == *:* ]]; then
    host_name=$(echo $first_server | cut -d':' -f1)
    port=$(echo $first_server | cut -d':' -f2)
  else
    host_name=$first_server
    port=80
  fi
echo "
    dynamic a $host_name $port {
      refresh   10s
    }
" >> /etc/caddy/Caddyfile
else
echo "
    to ${SERVERS}
" >> /etc/caddy/Caddyfile
fi

echo "
    lb_policy ${LB_POLICY}
    health_uri ${HEALTH_CHECK}
    health_status 2xx
" >> /etc/caddy/Caddyfile

if [[ "${ENABLE_BUFFERING}" == "true" ]]; then
echo "
    request_buffers ${MEM_REQUEST_BODY}
    response_buffers ${MEM_RESPONSE_BODY}
" >> /etc/caddy/Caddyfile
fi

if [[ "${ENABLE_AUTH}" == "true" ]]; then
echo "
    basic_auth {
      ${USER_NAME} $(caddy hash-password --plaintext ${USER_PASSWORD})
    }
" >> /etc/caddy/Caddyfile
fi

echo "
  }
" >> /etc/caddy/Caddyfile

if [[ "${ENABLE_BUFFERING}" == "true" ]]; then
echo "
  request_body {
    max_size ${MEM_REQUEST_BODY}
  }
" >> /etc/caddy/Caddyfile
fi

if [[ "${SSO}" == "true" ]]; then
echo "
  forward_auth swarmproxy-agent:5551 {
    uri /sso
  }
" >> /etc/caddy/Caddyfile
fi

echo "
}
" >> /etc/caddy/Caddyfile

caddy fmt --overwrite /etc/caddy/Caddyfile
cat /etc/caddy/Caddyfile
exec caddy run --config /etc/caddy/Caddyfile --adapter caddyfile
