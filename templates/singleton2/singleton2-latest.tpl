<@requirement.NODE ref='singleton' primary='singleton-${namespace}' single='false' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />
<@requirement.PARAM name='PUBLISHED_PORT_TYPE' values='global,local' value='global' type='select' depends='PUBLISHED_PORT' />

<@img.TASK 'singleton2-${namespace}' 'traefik/whoami'>
  <@img.NODE_REF 'singleton' />
  <@img.REPLICATED />
  <@img.PORT PARAMS.PUBLISHED_PORT '80' PARAMS.PUBLISHED_PORT_TYPE />
  <@img.CHECK_PORT '80' />
</@img.TASK>
