<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />
<@requirement.PARAM name='PUBLISHED_PORT_TYPE' values='global,local' value='global' type='select' depends='PUBLISHED_PORT' />

<@img.TASK 'singleton3-${namespace}' 'traefik/whoami'>
  <@img.MANAGER />
  <@img.REPLICATED />
  <@img.PORT PARAMS.PUBLISHED_PORT '80' PARAMS.PUBLISHED_PORT_TYPE />
  <@img.CHECK_PORT '80' />
</@img.TASK>
