<@requirement.NODE 'redis' 'redis-${namespace}' />

<@img.TASK 'redis-${namespace}' 'imagenarium/redis-stack-server:7.2.0-v11'>
  <@img.NODE_REF 'redis' />
  <@img.VOLUME '/data' />
  <@img.BIND '/sys/kernel/mm/transparent_hugepage' '/thp' />
  <@img.ULIMIT 'nofile=65536:65536' />
  <@img.ULIMIT 'nproc=4096:4096' />
  <@img.ULIMIT 'memlock=-1:-1' />
  <@img.SYSCTL 'net.core.somaxconn' '4096' />
  <@img.CUSTOM '--privileged=true' />
  <@img.ENV 'REDIS_ARGS' '--appendonly yes --aof-use-rdb-preamble yes --appendfsync always --protected-mode no --maxmemory 1gb' />
  <@img.CHECK_PORT '6379' />
</@img.TASK>
