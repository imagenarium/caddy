<@requirement.NODE ref='caddy' primary='caddy-${namespace}' single='false' />

<#-- Внешний порт балансировщика -->
<@requirement.PARAM name='PUBLISHED_PORT' value='80' type='port' />

<@requirement.PARAM name='ENABLE_HTTPS' value='false' type='boolean' />
<@requirement.PARAM name='PUBLISHED_HTTPS_PORT' value='443' type='port' depends='ENABLE_HTTPS' />
<@requirement.PARAM name='ACME_EMAIL' depends='ENABLE_HTTPS' />
<@requirement.PARAM name='DOMAIN_NAME' depends='ENABLE_HTTPS' description='Without www' />

<@requirement.PARAM name='UPSTREAMS_TYPE' values='STATIC,DYNAMIC' value='STATIC' type='select' description='STATIC - A list of upstreams, DYNAMIC - Retrieves upstreams from A/AAAA DNS records' />
<@requirement.PARAM name='SERVERS' description='my-microservice-ns1:9000 my-microservice-ns2:9000' />
<@requirement.PARAM name='HEALTH_CHECK' value='/' />
<@requirement.PARAM name='LB_POLICY' values='round_robin,cookie' value='round_robin' type='select' />

<@requirement.PARAM name='ENABLE_BUFFERING' value='true' type='boolean' />
<@requirement.PARAM name='MEM_REQUEST_BODY' value='20MB' depends='ENABLE_BUFFERING' />
<@requirement.PARAM name='MEM_RESPONSE_BODY' value='20MB' depends='ENABLE_BUFFERING' />

<@requirement.PARAM name='ENABLE_AUTH' value='false' type='boolean' />
<@requirement.PARAM name='USER_NAME' depends='ENABLE_AUTH' />
<@requirement.PARAM name='USER_PASSWORD' depends='ENABLE_AUTH' />


<@img.TASK 'caddy-${namespace}' 'imagenarium/caddy:2.8.4'>
  <#-- Ссылка на привязку к ноде -->
  <@img.NODE_REF 'caddy' />

  <@img.VOLUME '/data' />
  <@img.VOLUME '/config' />

  <#-- Проброс внешнего порта на внутренний порт контейнера -->
  <@img.PORT PARAMS.PUBLISHED_PORT '80' />

  <#if PARAMS.ENABLE_HTTPS == 'true'>
    <@img.PORT PARAMS.PUBLISHED_HTTPS_PORT '443' 'global' 'tcp' />
    <@img.PORT PARAMS.PUBLISHED_HTTPS_PORT '443' 'global' 'udp' />
  </#if>

  <@img.ENV 'ENABLE_HTTPS' PARAMS.ENABLE_HTTPS />
  <@img.ENV 'ACME_EMAIL' PARAMS.ACME_EMAIL />
  <@img.ENV 'DOMAIN_NAME' PARAMS.DOMAIN_NAME />

  <@img.ENV 'UPSTREAMS_TYPE' PARAMS.UPSTREAMS_TYPE />
  <@img.ENV 'SERVERS' PARAMS.SERVERS />
  <@img.ENV 'LB_POLICY' PARAMS.LB_POLICY />
  <@img.ENV 'HEALTH_CHECK' PARAMS.HEALTH_CHECK />

  <@img.ENV 'ENABLE_BUFFERING' PARAMS.ENABLE_BUFFERING />
  <@img.ENV 'MEM_REQUEST_BODY' PARAMS.MEM_REQUEST_BODY />
  <@img.ENV 'MEM_RESPONSE_BODY' PARAMS.MEM_RESPONSE_BODY />

  <@img.ENV 'ENABLE_AUTH' PARAMS.ENABLE_AUTH />
  <@img.ENV 'USER_NAME' PARAMS.USER_NAME />
  <@img.ENV 'USER_PASSWORD' PARAMS.USER_PASSWORD />

  <@img.CHECK_PORT '80' />
</@img.TASK>