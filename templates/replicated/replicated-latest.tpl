<@requirement.NODE ref='replicated' primary='replicated-${namespace}' single='false' />

<@requirement.PARAM name='PUBLISHED_PORT' required='false' type='port' />
<@requirement.PARAM name='PUBLISHED_PORT_TYPE' values='global,local' value='global' type='select' depends='PUBLISHED_PORT' />

<@img.TASK 'replicated-${namespace}' 'traefik/whoami'>
  <@img.NODE_REF 'replicated' />
  <@img.REPLICATED '2' />
  <@img.PORT PARAMS.PUBLISHED_PORT '80' PARAMS.PUBLISHED_PORT_TYPE />
  <@img.CHECK_PORT '80' />
</@img.TASK>
